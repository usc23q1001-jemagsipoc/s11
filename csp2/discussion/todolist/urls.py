from django.urls import path
from . import views

# Syntax
# path(route, view, name)

app_name = 'todolist'

urlpatterns = [
	path('', views.index, name='index'),
	# /todolist/<todoitem_id>
	# /todolist/1
	# The <int:todoitem_id> allows for creating a dynamic link where the todoitem_id is provided
	path('<int:todoitem_id>/', views.todoitem, name='viewtodoitem'),
	path('event/<int:todoevent_id>/', views.todoevent, name='viewtodoevent'),
	# /todolist/register
	path('register', views.register, name='register'),
	path('login', views.login_view, name="login"),
	path('logout', views.logout_view, name="logout"),
	path('update_profile', views.update_profile, name='update_profile'),
	path('add_task', views.add_task, name="add_task"),
	path('add_event', views.add_event, name="add_event"),
	path('<int:todoitem_id>/edit', views.update_task, name='update_task'),
	path('<int:todoitem_id>/delete',views.delete_task, name='delete_task')
]