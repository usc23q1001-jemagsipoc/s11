from django import forms

class LoginForm(forms.Form):
	username = forms.CharField(label='Username', max_length=20)
	password = forms.CharField(label='Password', max_length=20)

class updateProfileForm(forms.Form):
	firstName = forms.CharField(label='firstName', max_length=40)
	lastName = forms.CharField(label='lastName', max_length=40)
	password = forms.CharField(label='Password', max_length=20)

class AddTaskForm(forms.Form):
	task_name = forms.CharField(label='Task Name', max_length=50)
	description = forms.CharField(label='Description', max_length=200)

class AddEventForm(forms.Form):
	event_name = forms.CharField(label='Event Name', max_length=50)
	description = forms.CharField(label='Description', max_length=200)

class UpdateTaskForm(forms.Form):
	task_name = forms.CharField(label='Task Name', max_length=50)
	description = forms.CharField(label='Description', max_length=200)
	status = forms.CharField(label='Status', max_length=50)

class RegisterForm(forms.Form):
	username = forms.CharField(label='Username', max_length=20)
	firstName = forms.CharField(label='firstName', max_length=40)
	lastName = forms.CharField(label='lastName', max_length=40)
	email = forms.CharField(label='email', max_length=50)
	password = forms.CharField(label='Password', max_length=20)
	confirmPassword = forms.CharField(label='confirmPassword', max_length=20)

